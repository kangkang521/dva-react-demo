import { resolve } from 'path';

export default {
  theme: "./theme.config.js",
  alias: {
    themes: resolve(__dirname, './src/themes'),
    services: resolve(__dirname,"./src/services"),
    models: resolve(__dirname,"./src/models"),
    routes: resolve(__dirname,"./src/routes")
  }
}
