
const Mock = require('mockjs')

const apiPrefix = '/api'

let factoryHuoseData = Mock.mock({
  'data|80-100': [
    {
      id: '@id',
      name: '@name',
      number: '@natural(100,1000)',
      door: '@natural(1,9)',
      isChimney: '@boolean',
      createTime: '@datetime',
    },
  ],
})


let database = factoryHuoseData.data

const queryArray = (array, key, keyAlias = 'key') => {
  if (!(array instanceof Array)) {
    return null
  }
  let data

  for (let item of array) {
    if (item[keyAlias] === key) {
      data = item
      break
    }
  }

  if (data) {
    return data
  }
  return null
}


module.exports = {

  [`GET ${apiPrefix}/system/queryFactoryHuose`] (req, res) {
    const { query } = req
    let { pageSize, current, ...other } = query
    pageSize = pageSize || 10
    current = current || 1

    let newData = database
    for (let key in other) {
      if ({}.hasOwnProperty.call(other, key)) {
        newData = newData.filter((item) => {
          if ({}.hasOwnProperty.call(item, key)) {
            if (key === 'name' && other[key]) {
               
              return item[key].indexOf(other[key])>-1?true:false
            } else if ((key === 'number') && other[key]) {
                return Number(item[key])===Number(other[key])?true:false
            } else if ((key === 'door') && other[key]) {
                return Number(item[key])===Number(other[key])?true:false
            } else if ((key === 'isChimney')) {
                return Boolean(item[key])===Boolean(other[key])?true:false
            } else if (key === 'createTime' && other[key]) {
              const start = new Date(other[key][0]).getTime()
              const end = new Date(other[key][1]).getTime()
              const now = new Date(item[key]).getTime()

              if (start && end) {
                return now >= start && now <= end
              }
              return true
            }
            return String(item[key]).trim().indexOf(decodeURI(other[key]).trim()) > -1
          }
          return true
        })
      }
    }

    res.status(200).json({
      data: newData.slice((current - 1) * pageSize, current * pageSize),
      total: newData.length,
      current: current,
      pageSize: pageSize,
    })
  },

  [`DELETE ${apiPrefix}/system/deleteFactoryHuose/:id`] (req, res) {
    const { id } = req.params

    database = database.filter(item => Number(id) !== Number(item.id))
    res.status(200).end()
  },


  [`POST ${apiPrefix}/system/addFactoryHuose`] (req, res) {
    const newData = req.body
    newData.createTime = Mock.mock('@now')
    newData.id = Mock.mock('@id')
    database.unshift(newData)
    res.status(200).end()
  },

  [`GET ${apiPrefix}/system/queryFactoryHuose/:id`] (req, res) {
    const { id } = req.params
    const data = queryArray(database, id, 'id')
    res.status(200).json(data)
  },

  [`PUT ${apiPrefix}/system/updateFactoryHuose/:id`] (req, res) {
      const { id } = req.params

    const editItem = req.body
    database = database.map((item) => {
      if (item.id === id) {
        return Object.assign({}, item, editItem)
      }
      return item
    })
    res.status(200).end()
  },
}
