import React,{ Component } from 'react'
import BestHouse from './components/BestHouse'
// import House from './components/House'
// import HouseChimney from './components/HouseChimney'
let houseName = ""
let doorNum = ""
let isChimney = false

export default class componentState extends Component {
  /**
   * 管理整个页面的 state ，state 控制着整个页面的渲染。  
   **/  
  state = {
      factory : [ ],
      }
      /**
       * 箭头函数，与 function 函数一样
       */
      createHose = () => {
        const factory = this.state.factory
        factory.push({name: houseName.value,door: { number: doorNum.value }, isChimney: isChimney.value })
        this.setState({
          factory:[...factory],
        })
      }
  render (){
      return (
        <div style={{ textAlign: 'center' }}>
            {/* ref 特性，可以绑定 render 出来的任何组件 */}
            房子名称：<input ref={(_)=>houseName=_}/> 门的数量：<input ref={(_)=>doorNum=_}/>是否有烟筒：<select ref={(_)=>isChimney=_}><option value={false}>false</option><option value={true}>true</option></select>
            {/* 事件特性，jQyery 写法是 onclick , React 写法 onClick */}
            <button onClick={this.createHose}>建造房子</button>
            <div  style={{ textAlign: 'center', fontSize: 25 }}>
              {this.state.factory.map((item)=><BestHouse key={item.name} {...{name:item.name, door: { number: item.door.number }, isChimney: item.isChimney }}/>)}
            </div>
        </div>)
  }
}

