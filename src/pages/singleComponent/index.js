import React,{ Component } from 'react'

//在外部创建一个组件
import HelloBoy from './HelloBoy'

//以 JSX 结尾创建一个组件
import HelloGirl from './HelloGirl'

export default class Home extends Component {
    state = {
      }

  render (){
    //   创建一个组件
      const HelloWord = () => {return <div>HelloWord</div>}
    //   创建一个变量形式的组件
      const HelloFriend = <div>hello Friend!</div>
      return (
        <div style={{ textAlign: 'center', fontSize: 25 }}>
            {/* 使用一个内部创建的组件 */}
            <HelloWord></HelloWord>
            {/* 使用一个变量创建的组件 */}
            {HelloFriend}
            {/* 使用一个外部创建的组件 */}
            <HelloBoy></HelloBoy>
            {/* 使用一个外部以 JSX 结尾的文件创建一个组建 */}
            <HelloGirl></HelloGirl>
        </div>)
  }
}

