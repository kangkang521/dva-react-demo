import React,{ Component  } from 'react'
import Example from './components/Example'

export default class Home extends Component  {
    state = {
        number: 0,
        flag: true,
      }
   
  render (){
    const myThis = this
      return (
        <div style={{ textAlign: 'center', fontSize: 25 }}>
            <button onClick={()=>{
                this.setState({
                    number: myThis.state.number+1,
                })
            }}>我是一个按钮</button>
            <br/>
            <button onClick={()=>{
                this.setState({
                    flag: myThis.state.flag?false:true,
                })
            }}>我是卸载组建的按钮</button>
            <h3>----------------------------------我是一个分界线-------------------------------</h3>
            {this.state.flag && (<Example {...this.state}/>)}
        </div>)
  }
}

