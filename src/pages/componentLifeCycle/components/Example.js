import React,{ Component } from 'react'

export default class Example extends Component {
   // eslint-disable-next-line
   componentWillMount () {
    console.log('在页面 render（渲染） 之前执行此方法!')
    }
    componentDidMount () {
        console.log('在页面第一次 render（渲染） 之后执行此方法!')
    }
    // eslint-disable-next-line
    componentWillReceiveProps (newProps) {
        console.log('在组件接收到一个新的 prop (更新后)时被调用。这个方法在初始化render时不会被调用!', newProps)
    }
    shouldComponentUpdate (newProps, newState) {
        console.log(newProps, newState)
        console.log('shouldComponentUpdate')
        if(newProps.number > 5){
            return false
        }
        return true
    }
    // eslint-disable-next-line
    componentWillUpdate (nextProps, nextState) {
        console.log(nextProps, nextState)
        console.log('在组件接收到新的props或者state但还没有render时被调用。在初始化时不会被调用',)
    }
    componentDidUpdate (prevProps, prevState) {
        console.log(prevProps, prevState)
        console.log('在组件完成更新后立即调用。在初始化时不会被调用。')
    }
    componentWillUnmount () {
        console.log('在组件从 DOM 中移除的时候立刻被调用!')
    }
  render (){
      const { number } = this.props
      return (<div>{number}</div>)
  }
}

