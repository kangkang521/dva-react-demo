import React, { Component } from 'react'
import { Form, Button, Row, Col, Select, Input } from 'antd'

const FormItem = Form.Item
const Option = Select.Option
const { Search } = Input

const ColProps = {
    sm: 24, md: 7,
}

const TwoColProps = {
  ...ColProps, 
  sm: 24, md: 14,
}
const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }

@Form.create()
export default class Filter extends Component {

  handleSubmit = () => {
    const { getFieldsValue } = this.props.form
    let fields = getFieldsValue()
    this.props.onFilterChange(fields)
  }


    render (){
        const { getFieldDecorator } = this.props.form
        return (
            <Form>
              <Row>
                <Col {...ColProps} >
                  <FormItem {...formItemLayout} {...formItemLayout} label="房子编号">
                    {getFieldDecorator('number', { initialValue: '' })(<Search placeholder="请输入房子编号"/>)}
                  </FormItem>
                </Col>
                <Col {...ColProps} >
                  <FormItem {...formItemLayout} label="房子名称">
                    {getFieldDecorator('name', { initialValue: '' })(<Search placeholder="请输入房子名称"/>)}
                  </FormItem>
                </Col>
                <Col {...ColProps} >
                  <FormItem {...formItemLayout} label="有几扇门">
                    {getFieldDecorator('door', { initialValue: '' })(<Search placeholder="请输入几扇门"/>)}
                  </FormItem>
                </Col>
            </Row>
            <Row>
                <Col {...ColProps} >
                  <FormItem {...formItemLayout} label="是否有烟筒">
                    {getFieldDecorator('isChimney')(<Select
                      allowClear
                      placeholder="请选择是否有烟筒"
                    >
                      <Option value="true">有</Option>
                      <Option value="false">没有</Option>
                    </Select>)}
                  </FormItem>
                </Col>
                <Col {...TwoColProps} style={{ paddingLeft: '10px' }}>
                  <div style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap', textAlign: 'right' }}>
                    <div className="flex-vertical-center">
                      
                    </div>
                    <div className="flex-vertical-center">
                      <Button type="primary" className="margin-right" icon="search" onClick={this.handleSubmit}>查询</Button>
                      <Button type="ghost" className="margin-right" icon="retweet">重置</Button>
                      <Button type="primary" icon="plus" onClick={this.props.onAdd}>建造房子</Button>
                    </div>
                  </div>
                </Col>
              </Row>
        </Form >
      )
    }
}
