import React, { Component } from 'react'
import { Form, Row, Col, Select, Input, Modal } from 'antd'

const FormItem = Form.Item
const Option = Select.Option

const ColProps = {
    sm: 24, md: 24,
}

const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 18 },
    },
}

@Form.create()
export default class Huose extends Component {

  
handleSubmit = () => {
  this.props.form.validateFields((err, values) => {
    if (!err) {
      this.props.onOk(values)
    }
  })
}



    render (){
        const { getFieldDecorator } = this.props.form
        const { visible, onCancel, title, rowData } = this.props
        const { name, number, door, isChimney} = rowData
        const modalOpts = {
          title: title,
          onCancel: onCancel,
          onOk: this.handleSubmit,
          visible: visible,
        }
        return (
          <Modal {...modalOpts}>
            <Form>
              <Row>
                <Col {...ColProps} >
                  <FormItem {...formItemLayout} {...formItemLayout} label="房子编号">
                    {getFieldDecorator('number', { 
                      initialValue: number,
                      rules: [
                        { required: true, message: '房子编号必须输入!' },
                      ],
                      })(<Input placeholder="请输入房子编号"/>)}
                  </FormItem>
                </Col>
                <Col {...ColProps} >
                  <FormItem {...formItemLayout} label="房子名称">
                    {getFieldDecorator('name', { 
                        initialValue: name,
                        rules: [
                          { required: true, message: '房子名称必须输入!' },
                        ],
                          })(<Input placeholder="请输入房子名称"/>)}
                  </FormItem>
                </Col>
                <Col {...ColProps} >
                  <FormItem {...formItemLayout} label="几扇门">
                    {getFieldDecorator('door', { initialValue: door })(<Input placeholder="请输入几扇门"/>)}
                  </FormItem>
                </Col>
                <Col {...ColProps} >
                  <FormItem {...formItemLayout} label="是否有烟筒">
                    {getFieldDecorator('isChimney', { initialValue: isChimney })(<Select
                      allowClear
                      placeholder="请选择是否有烟筒"
                    >
                      <Option value={true}>有</Option>
                      <Option value={false}>没有</Option>
                    </Select>)}
                  </FormItem>
                </Col>
              </Row>
         </Form >
        </Modal>
      )
    }
}
