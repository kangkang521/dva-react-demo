import React,{ Component } from 'react'
import { Table, Button, Modal } from 'antd'


const confirm = Modal.confirm

export default class FactoryHuose extends Component {
    render (){
        const columns = [
            {
              title: '序号',
              dataIndex: 'index',
              key: 'index',
              width: 60,
              render: (text, record, index) => <span>{this.props.pagination.pageSize * (this.props.pagination.current - 1) + index + 1}</span>,
            },
            {
                title: '编号',
                dataIndex: 'number',
                key: 'number',
              },
              {
                title: '名称',
                dataIndex: 'name',
                key: 'name',
              },
              {
                title: '有几扇门',
                dataIndex: 'door',
                key: 'door',
              },
              {
                title: '是否有烟筒',
                dataIndex: 'isChimney',
                key: 'isChimney',
                render: (text) => <span>{text?'有':'没有'}</span>,
              },
              {
                title: '操作',
                render: (record) => <div>
                  <Button size="small" type="danger" onClick={()=>{
                    const myThis = this
                    confirm({
                      title: `确定要删除？`,
                      content: <div><p style={{ fontSize: 16, fontWeight: 'bold' }}>名称：{record.name}</p><p style={{ fontSize: 16, fontWeight: 'bold', color:'red' }}>数据删除后，将无法恢复！</p></div>,
                      okText: '确定删除',
                      okType: 'danger',
                      cancelText: '取消',
                      onOk () {
                        myThis.props.deleteRow(record.id) 
                      },
                    })
                    
                  }}>删除</Button>
                  &nbsp;&nbsp;
                  <Button size="small" onClick={()=>{
                    this.props.onUpdate(record)
                  }}>编辑</Button>
                  </div>,
              },
        ]
        return (
            <Table {...this.props} pagination={this.props.pagination} columns={columns} rowKey={record => record.id}/>
        )
    }
}
FactoryHuose.propTypes = {
}
