/**
 * 封装的一个类似 ajax 接口的方法
 */
/**
 * 可以做一个配置文件方便管理
 */
import { config, request } from 'utils'

const { updateFactoryHuoseById_, queryFactoryHuose_, queryFactoryHuoseById_ } = config
/**
 * 用post方法进行添加一个房子的接口
 * @param params 参数
 */
export function addFactoryHuose (params) {
  return request({
    url: '/api/system/addFactoryHuose',
    method: 'post',
    data: params,
  })
}
/**
 * 根据id用delete方法进行删除一个房子的接口
 * @param params 参数
 */
export function deleteFactoryHuoseById (params) {
  return request({
    url: `/api/system/deleteFactoryHuose/${params.id}`,
    method: 'delete',
  })
}

export function updateFactoryHuoseById (params) {
  return request({
    url: updateFactoryHuoseById_.replace(':id', params.id),
    method: 'put',
    data: params,
  })
}

export function queryFactoryHuose (params) {
    return request({
      url: queryFactoryHuose_,
      method: 'get',
      data: params,
    })
  }

export function queryFactoryHuoseById (params) {
  return request({
    url: queryFactoryHuoseById_.replace(':id', params.id),
    method: 'get',
    data: params,
  })
}
