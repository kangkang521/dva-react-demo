/**
 * 导入我们在 service 里定义的--接口方法
 */
import { addFactoryHuose,
         deleteFactoryHuoseById,
         updateFactoryHuoseById,
         queryFactoryHuose,
         queryFactoryHuoseById } from '../services/FactoryHuose'

export default {
    /**
     * 全局的命名空间，必须是全局唯一的名字
     * 改名字会在连接页面时用到
     */
    namespace: 'factoryHuose',
    /**
     * 存储需要连接的页面组建的状态及数据
     * state 为一个 objectTree 结构类型数据
     * 例如： 
     * state: {
     *    name: '',
     *    user:{ name: '' }
     *    listUser: [{ user: { name:'' } }]
     * }
     */
    state: {
        factory : [ ],
        visible : false,
        pagination: {},
        modalType: undefined,
        rowData: {},
    },
    /**
     * 向后端应用请求一个异步的接口的调用
     * 例如：
     *  1、查询后端数据
     *  2、更改某一条数据
     *  3、删除某一条数据
     * 
     * 总之跟后端（服务端）交互的操作都在这里面完成
     * 
     * 写法如下
     */
    effects: {
        /**
         * 
         * @param {*} payload   请求action时传过来的数据参数
         * @param {*} callback  回调函数
         * @function call 异步去请求一个接口
         * @function put  可以请求一个 action 去改变 state 的值，然后去渲染页面
         */
        *addFactoryHuose ({ payload = {}, callback }, { call }){
            /**
             * yield 关键字，暂停的意思，意在把一个异步的请求 call 同步化
             * call(payload) 发送一个异步请求
             * response 异步接口返回的数据
             */
            const response = yield call(addFactoryHuose,payload)
            /**
             * 去判断一下是否有回调函数，若有则会回调一下并把接口返回的数据返回
             */
            if(callback){callback(response)}
        },
        /**
         * 
         * @param {*} payload 
         * @param {*} callback 
         */
        *deleteFactoryHuoseById ({ payload = {}, callback }, { call }){
            const response = yield call(deleteFactoryHuoseById,payload)
            if(callback){callback(response)}
        },
        *updateFactoryHuoseById ({ payload = {}, callback }, { call }){
            const response = yield call(updateFactoryHuoseById,payload)
            if(callback){callback(response)}
        },
        *queryFactoryHuose ({ payload = {}, callback }, { call, put }){
            const response = yield call(queryFactoryHuose,payload)
            yield put({
                type: 'updateState',
                payload: {
                    factory: [...response.data],
                    pagination: {
                        showSizeChanger: true,
                        showQuickJumper: true,
                        showTotal: total => `共     ${total} 条`,
                        current: Number(response.current),
                        total: response.total,
                        pageSize: Number(response.pageSize),
                      },
                },
            })
            if(callback){callback(response)}
        },
        *queryFactoryHuoseById ({ payload = {}, callback }, { call, put }){
            const response = yield call(queryFactoryHuoseById,payload)
            yield put({
                type: '',
                payload: response,
            })
            if(callback){callback(response)}
        },
    },
    /**
     * 意在管理 state 里面的数据
     * 若想改变 state 里面的数据则只有通过调用此方法进行改变
     * 且该 state 的改变可以直接影响 React 页面渲染。
     *  
     */ 
    reducers: {
        /**
         * 
         * @param {*} state 该 modal 内部 state 存储的所有数据
         * @param {*} action 传来的参数
         */
        showModal (state, action){
            return {
                ...state,
                ...action.payload,
                ...{visible: true},
            }
        },
        /**
         * 
         * @param {*} state 该 modal 内部 state 存储的所有数据
         * @param {*} action 传来的参数
         */
        closeModal (state, action){
            return {
                ...state,
                ...action.payload,
                ...{visible: false},
            }
        },
        /**
         * 
         * @param {*} state 该 modal 内部 state 存储的所有数据
         * @param {*} action 传来的参数
         */
        updateState (state, action){
            return {
                ...state,
                ...action.payload,
            }
        },
    },
}