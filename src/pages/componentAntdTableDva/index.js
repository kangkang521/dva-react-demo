import React,{ Component } from 'react'
import FactoryHuose from './component/FactoryHuose'
import Filter from './component/Filter'
import Huose from './component/Huose'
import { connect } from 'dva'

@connect(({ factoryHuose/* 这是一个 namespace */, loading }) => ({
  factoryHuose,
  loading: loading,
}))
export default class index extends Component {
  componentDidMount () {
    const { dispatch } = this.props
    dispatch({
      type: 'factoryHuose/queryFactoryHuose',
      payload:{
        current:1,
        pageSize:10,
      },
    })
  }
      
  render (){
    console.log( this.props)
    const { factoryHuose: { factory, visible, pagination, modalType, rowData }, dispatch , loading } = this.props
      const tables = {
        dataSource: factory,
        pagination: pagination,
        loading: loading.effects['factoryHuose/queryFactoryHuose'],
        deleteRow (id){
          dispatch({
            type: 'factoryHuose/deleteFactoryHuoseById',
            payload:{
              id:id,
            },
            callback:()=>{
              dispatch({
                type: 'factoryHuose/queryFactoryHuose',
                payload:{
                  current: 1,
                  pageSize: 10,
                },
              })
            },
          })
        },
        onUpdate (record){
          dispatch({
            type: 'factoryHuose/showModal',
            payload:{
              rowData:{...record},
              modalType: 'update',
            },
          })
        },
        onChange (page) {
          dispatch({
            type: 'factoryHuose/queryFactoryHuose',
            payload:{
              current: Number(page.current),
              pageSize: Number(page.pageSize),
            },
          })
        },
      }

      const filterProps = {
        onAdd (){
          dispatch({
            type: 'factoryHuose/showModal',
            payload:{
              modalType: 'add',
              rowData: {},
            },
          })
        },
        onFilterChange (value) {
          dispatch({
            type: 'factoryHuose/queryFactoryHuose',
            payload:{
              ...value,
              current: 1,
              pageSize: 10,
            },
          })
        },
      }
      const huoseProps = {
        title: '建造房子',
        visible: visible,
        rowData: rowData,
        onOk (huose){
          const { id } = rowData
          dispatch({
            type: `${modalType === 'add'?'factoryHuose/addFactoryHuose':'factoryHuose/updateFactoryHuoseById'}`,
            payload:{...huose,id: id},
            callback: (data)=>{
              if(data.success){
                dispatch({
                  type: 'factoryHuose/closeModal',
                  payload:{
                    modalType: undefined,
                    rowData: {},
                  },
                })
                dispatch({
                  type: 'factoryHuose/queryFactoryHuose',
                  payload:{
                    current: 1,
                    pageSize: 10,

                  },
                })
                
              }
            },
          })
        },
        onCancel (){
          dispatch({
            type: 'factoryHuose/closeModal',
            payload:{
              modalType: undefined,
            },
          })
        },
      }
      return (
        <div style={{ textAlign: 'center', backgroundColor: '#FFF' }}>
          <Filter {...filterProps}/>
          <FactoryHuose {...tables} />
          {modalType && <Huose {...huoseProps}/>}
        </div>)
  }
}

