/**
 * 导入一个请求的库
 */
import { request } from 'utils'
/**
 * 导出一个叫 query 的接口方法，那么他的参数为  params  
 */
export function query (params){
    /**
     * 根据接口文档提供的接口需求进行请求接口
     * 
     * 接口文档会提供
     *  xxx 功能
     *  请求类型 xxx
     *  请求的Url xxx
     *  需要传的参数 xxx
     * 
     */
    request({
        url:'',
        method:'',
        data:params,
    })
} 