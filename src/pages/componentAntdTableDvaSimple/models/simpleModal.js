export default {
    /**
     * namespace 全局唯一的命名空间
     */
    namespace:'simpleModal',
    /**
     * 管理整个页面的状态，纳管了页面中所有组建的 state,一旦 state 的值发生改变，那么页面相应的则会发生重新渲染。
     */
    state: {
        visible: false,
        title: '我是弹出框的标题',
    },
    /**
     * 向服务端发送一个异步请求，类似 jQuery 的 Ajax 的方式去异步的请求一个数据接口。
     */
    effects: {},
    /**
     * 一个纯函数，意在处理 state 的值，若想更改 state 值，那么只有在这里进行更改。
     */
    reducers:{
        updateState (state, action){
            return {
                ...state,
                ...action.payload,
            }
        },
    },
}