import React,{ Component } from 'react'
import MyModal from './component/MyModal'
import { Button } from 'antd'
/**
 * 导入 Dva 的连接属性
 */
import { connect } from 'dva'

/**
 * 把本页面连接到一个 modals
 * 根据 modals 里面的 namespace 进行连接
 * loading 是 Dva 内置的对异步请求的一个请求状态
 */
@connect(({ simpleModal/* 这是一个 namespace */, loading }) => ({
    simpleModal,
    loading: loading.models.simpleModal,
}))
export default class  componentAntdTableDvaSimple extends Component {


    showMyModal = () => {
        const { dispatch } = this.props
        dispatch({
            type: 'simpleModal/updateState',
            payload:{
                visible: true,
            },
        })
    }

    render (){
        /**
         * 经过一个在类上进行连接了一个 modals 那么该页面的 this.props 相应的就会有一些 Dva 带过来的属性
         */
        console.log(this.props)/** 对this.props 进行打印 */
        const { simpleModal, dispatch } = this.props /**从 this.props 里面取出来 state 的值 */
        console.log('simpleModal', simpleModal) /** 对取出来的值进行打印 */


        const { visible, title } = simpleModal

        const myModalProps = {
            visible: visible,
            title: title,
            cancelText: '关闭',
            onOk (){
            },
            onCancel (){
                dispatch({
                    type: 'simpleModal/updateState',
                    payload:{
                        visible: false,
                    },
                })
            },
        }
        return <div  style={{ textAlign: 'center', fontSize: 25 }}>
            <Button type="primary" onClick={this.showMyModal}>点击我弹出一个窗口</Button>
            <MyModal {...myModalProps}/>
        </div>
    }
}