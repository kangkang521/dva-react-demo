import React,{ Component } from 'react'
// import House from './components/House'
import ADoor from './components/ADoor'
import AWindow from './components/AWindow'
import Chimney from './components/Chimney'

export default class Home extends Component {
    state = {
      }
  render (){
    //建造一个有门、有窗户的房子
    const House = () => {
      return  <div>
                我是一座有窗户、有门的房子！
                {/* 这是一扇门 */}
                <ADoor/>
                {/* 这是一扇窗户 */}
                <AWindow/>
              </div>
    }
    //建造一个有门、有窗户、有烟筒的房子
    const HouseHaveChimney = () => {
      return <div>
              我是一座有窗户、有门、有烟筒的房子！
              {/* 这是一扇门 */}
              <ADoor/>
              {/* 这是一扇窗户 */}
              <AWindow/>
              {/* 这是一个烟筒 */}
              <Chimney/>
             </div>
    }


      return (
        <div style={{ textAlign: 'center', fontSize: 25 }}>
            <House/> 
            <HouseHaveChimney/>          
        </div>)
  }
}

