import React,{ Component } from 'react'
import FactoryHuose from './component/FactoryHuose'
import Filter from './component/Filter'
import Huose from './component/Huose'

export default class index extends Component {
    state = {
      factory : [ ],
      visible : false,
      }
      

      
  render (){
    const myThis = this
      const tables = {
        dataSource: this.state.factory,
        deleteRow (number){
          const factory = myThis.state.factory
          myThis.setState({
            factory: [...factory.filter(_=>_.number !== number)],
          })
        },
      }

      const filterProps = {
        onAdd (){
          myThis.setState({visible: true})
        },
      }
      const huoseProps = {
        title: '建造房子',
        visible: this.state.visible,
        onOk (huose){
          const factory = myThis.state.factory
          factory.push(huose)
          myThis.setState({
            factory: [...factory],
          })
          myThis.setState({visible: false})
        },
        onCancel (){
          myThis.setState({visible: false})
        },
      }
      return (
        <div style={{ textAlign: 'center', backgroundColor: '#FFF' }}>
          <Filter {...filterProps}/>
          <FactoryHuose {...tables} />
          <Huose {...huoseProps}/>
        </div>)
  }
}

