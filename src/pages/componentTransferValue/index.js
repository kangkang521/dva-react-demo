import React,{ Component } from 'react'
import BestHouse from './components/BestHouse'
// import House from './components/House'
// import HouseChimney from './components/HouseChimney'

const factory = [
          {name: '1号房子',door: { number: '一' }, isChimney: false },
          {name: '2号房子',door: { number: '两' }, isChimney: true },
          {name: '3号房子',door: { number: '三' }, isChimney: false },
          {name: '4号房子',door: { number: '四' }, isChimney: true },
        ]

export default class Home extends Component {
    state = {
      }
      
  render (){

    // 建造一个厕所
    // const Toilet = (props) => {
    //   return <div>{props.name}</div>
    // }


      return (
        <div style={{ textAlign: 'center', fontSize: 25 }}>
            {/* <House {...{name: '我是东屋！', door: { number: '三' }}}/>
            <HouseChimney {...{name: '我是厨房！'}}/> */}
            <h4>-------------------------</h4>
            {/* <House {...{name: '我是西屋！'}}/>
            <h4>-------------------------</h4>
            <House {...{name: '我是北屋！'}}/>
            <h4>-------------------------</h4>
               
            <h4>-------------------------</h4>
            <Toilet {...{name: '我是一个厕所！'}}/>        */} */}
             {/* <BestHouse {...{name: '我是东屋！', door: { number: '两' }, isChimney: true }}/> */}
            
            {/* map 函数 声明式写法，注重结果，不注重实现！ */}
           {factory.map((item)=>
            <BestHouse key={item.name} {...{name:item.name, door: { number: item.door.number }, isChimney: item.isChimney }}/>
           )}
        </div>)
  }
}

