import React,{ Component } from 'react'
import ADoor from './ADoor'
import AWindow from './AWindow'
import Chimney from './Chimney'

export default class BestHouse extends Component {
    state = {
      }

  render (){
      /** 析构 */
      const { name, door, isChimney } = this.props
      return (<div>
                {name}
                {/* 这是一扇门 */}
                {/* ... 扩展运算符 （个人俗称三个点）功能强大  */}
                <ADoor {...door}/>
                {/* 这是一扇窗户 */}
                <AWindow/>
                {isChimney && <Chimney/>}
             </div>)
  }
}

