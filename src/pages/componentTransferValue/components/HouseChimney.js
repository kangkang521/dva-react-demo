import React,{ Component } from 'react'
import ADoor from './ADoor'
import AWindow from './AWindow'
import Chimney from './Chimney'

export default class HouseChimney extends Component {
    state = {
      }

  render (){
      const { name } = this.props
      return (<div>
                {name}
                {/* 这是一扇门 */}
                <ADoor/>
                {/* 这是一扇窗户 */}
                <AWindow/>
                {/* 这是一个烟筒 */}
                <Chimney/>
             </div>)
  }
}

