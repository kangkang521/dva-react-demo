import React,{ Component } from 'react'

export default class ADoor extends Component {
    state = {
      }

  render (){
      /** 析构 */
      const { number } = this.props
      return (<div>
                {/* 模板字符串 `` */}
                {/* 三目运算符 true/false ? true:false */}
                {/* && ||  */}
                {number? `我有${number}扇门`:`我有一扇门`}
              </div>)
  }
}

