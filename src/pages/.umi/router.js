import React from 'react';
import { Router as DefaultRouter, Route, Switch } from 'react-router-dom';
import dynamic from 'umi/dynamic';
import renderRoutes from 'umi/_renderRoutes';
import { routerRedux } from 'dva/router';



let Router = DefaultRouter;
const { ConnectedRouter } = routerRedux;
Router = ConnectedRouter;


let routes = [
  {
    "path": "/",
    "component": require('../../layouts/index.js').default,
    "routes": [
      {
        "path": "/",
        "exact": true,
        "component": require('../index.js').default
      },
      {
        "path": "/componentAntdTableDva/component/Filter",
        "exact": true,
        "component": require('../componentAntdTableDva/component/Filter.js').default
      },
      {
        "path": "/componentAntdTableDva/component/Huose",
        "exact": true,
        "component": require('../componentAntdTableDva/component/Huose.js').default
      },
      {
        "path": "/componentAntdTableDva",
        "exact": true,
        "component": require('../componentAntdTableDva/index.js').default
      },
      {
        "path": "/componentAntdTableDvaSimple/component/MyModal",
        "exact": true,
        "component": require('../componentAntdTableDvaSimple/component/MyModal.js').default
      },
      {
        "path": "/componentAntdTableDvaSimple",
        "exact": true,
        "component": require('../componentAntdTableDvaSimple/index.js').default
      },
      {
        "path": "/componentDecorate",
        "exact": true,
        "component": require('../componentDecorate/index.js').default
      },
      {
        "path": "/componentLifeCycle",
        "exact": true,
        "component": require('../componentLifeCycle/index.js').default
      },
      {
        "path": "/componentAntdTable/component/FactoryHuose",
        "exact": true,
        "component": require('../componentAntdTable/component/FactoryHuose.js').default
      },
      {
        "path": "/componentAntdTable/component/Filter",
        "exact": true,
        "component": require('../componentAntdTable/component/Filter.js').default
      },
      {
        "path": "/componentAntdTable/component/Huose",
        "exact": true,
        "component": require('../componentAntdTable/component/Huose.js').default
      },
      {
        "path": "/componentAntdTable",
        "exact": true,
        "component": require('../componentAntdTable/index.js').default
      },
      {
        "path": "/componentAntdTableDva/component/FactoryHuose",
        "exact": true,
        "component": require('../componentAntdTableDva/component/FactoryHuose.js').default
      },
      {
        "path": "/componentTransferValue",
        "exact": true,
        "component": require('../componentTransferValue/index.js').default
      },
      {
        "path": "/compositeComponent",
        "exact": true,
        "component": require('../compositeComponent/index.js').default
      },
      {
        "path": "/componentAntd",
        "exact": true,
        "component": require('../componentAntd/index.js').default
      },
      {
        "path": "/singleComponent/HelloBoy",
        "exact": true,
        "component": require('../singleComponent/HelloBoy.js').default
      },
      {
        "path": "/singleComponent/HelloGirl",
        "exact": true,
        "component": require('../singleComponent/HelloGirl.jsx').default
      },
      {
        "path": "/singleComponent",
        "exact": true,
        "component": require('../singleComponent/index.js').default
      },
      {
        "path": "/componentState",
        "exact": true,
        "component": require('../componentState/index.js').default
      },
      {
        "component": () => React.createElement(require('M:/work/eclipse-workspace/dva-react-demo/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', routes: '[{"path":"/","component":"./src\\\\layouts\\\\index.js","routes":[{"path":"/componentTransferValue/components/AWindow","exact":true,"component":"./src/pages/componentTransferValue/components/AWindow.js"},{"path":"/","exact":true,"component":"./src/pages/index.js"},{"path":"/componentAntdTableDva/component/Filter","exact":true,"component":"./src/pages/componentAntdTableDva/component/Filter.js"},{"path":"/componentAntdTableDva/component/Huose","exact":true,"component":"./src/pages/componentAntdTableDva/component/Huose.js"},{"path":"/componentAntdTableDva","exact":true,"component":"./src/pages/componentAntdTableDva/index.js"},{"path":"/componentAntdTableDva/models/FactoryHuose","exact":true,"component":"./src/pages/componentAntdTableDva/models/FactoryHuose.js"},{"path":"/componentAntdTableDva/services/FactoryHuose","exact":true,"component":"./src/pages/componentAntdTableDva/services/FactoryHuose.js"},{"path":"/componentAntdTableDvaSimple/component/MyModal","exact":true,"component":"./src/pages/componentAntdTableDvaSimple/component/MyModal.js"},{"path":"/componentAntdTableDvaSimple","exact":true,"component":"./src/pages/componentAntdTableDvaSimple/index.js"},{"path":"/componentAntdTableDvaSimple/models/simpleModal","exact":true,"component":"./src/pages/componentAntdTableDvaSimple/models/simpleModal.js"},{"path":"/componentAntdTableDvaSimple/services/simpleService","exact":true,"component":"./src/pages/componentAntdTableDvaSimple/services/simpleService.js"},{"path":"/componentDecorate","exact":true,"component":"./src/pages/componentDecorate/index.js"},{"path":"/componentLifeCycle/components/Example","exact":true,"component":"./src/pages/componentLifeCycle/components/Example.js"},{"path":"/componentLifeCycle","exact":true,"component":"./src/pages/componentLifeCycle/index.js"},{"path":"/componentAntdTable/component/FactoryHuose","exact":true,"component":"./src/pages/componentAntdTable/component/FactoryHuose.js"},{"path":"/componentAntdTable/component/Filter","exact":true,"component":"./src/pages/componentAntdTable/component/Filter.js"},{"path":"/componentAntdTable/component/Huose","exact":true,"component":"./src/pages/componentAntdTable/component/Huose.js"},{"path":"/componentAntdTable","exact":true,"component":"./src/pages/componentAntdTable/index.js"},{"path":"/componentTransferValue/components/ADoor","exact":true,"component":"./src/pages/componentTransferValue/components/ADoor.js"},{"path":"/componentAntdTableDva/component/FactoryHuose","exact":true,"component":"./src/pages/componentAntdTableDva/component/FactoryHuose.js"},{"path":"/componentTransferValue/components/BestHouse","exact":true,"component":"./src/pages/componentTransferValue/components/BestHouse.js"},{"path":"/componentTransferValue/components/Chimney","exact":true,"component":"./src/pages/componentTransferValue/components/Chimney.js"},{"path":"/componentTransferValue/components/House","exact":true,"component":"./src/pages/componentTransferValue/components/House.js"},{"path":"/componentTransferValue/components/HouseChimney","exact":true,"component":"./src/pages/componentTransferValue/components/HouseChimney.js"},{"path":"/componentTransferValue","exact":true,"component":"./src/pages/componentTransferValue/index.js"},{"path":"/compositeComponent/components/ADoor","exact":true,"component":"./src/pages/compositeComponent/components/ADoor.js"},{"path":"/compositeComponent/components/AWindow","exact":true,"component":"./src/pages/compositeComponent/components/AWindow.js"},{"path":"/compositeComponent/components/Chimney","exact":true,"component":"./src/pages/compositeComponent/components/Chimney.js"},{"path":"/compositeComponent","exact":true,"component":"./src/pages/compositeComponent/index.js"},{"path":"/componentAntd","exact":true,"component":"./src/pages/componentAntd/index.js"},{"path":"/singleComponent/HelloBoy","exact":true,"component":"./src/pages/singleComponent/HelloBoy.js"},{"path":"/singleComponent/HelloGirl","exact":true,"component":"./src/pages/singleComponent/HelloGirl.jsx"},{"path":"/singleComponent","exact":true,"component":"./src/pages/singleComponent/index.js"},{"path":"/componentState/components/ADoor","exact":true,"component":"./src/pages/componentState/components/ADoor.js"},{"path":"/componentState/components/AWindow","exact":true,"component":"./src/pages/componentState/components/AWindow.js"},{"path":"/componentState/components/BestHouse","exact":true,"component":"./src/pages/componentState/components/BestHouse.js"},{"path":"/componentState/components/Chimney","exact":true,"component":"./src/pages/componentState/components/Chimney.js"},{"path":"/componentState","exact":true,"component":"./src/pages/componentState/index.js"}]}]' })
      }
    ]
  }
];


export default function() {
  return (
<Router history={window.g_history}>
  <Route render={({ location }) =>
    renderRoutes(routes, {}, { location })
  } />
</Router>
  );
}
