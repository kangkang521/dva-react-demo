import { Component } from 'react';
import dva from 'dva';
import createLoading from 'dva-loading';

let app = dva({
  history: window.g_history,
  
});

window.g_app = app;
app.use(createLoading());

app.model({ ...(require('M:/work/eclipse-workspace/dva-react-demo/src/models/app.js').default) });
app.model({ ...(require('M:/work/eclipse-workspace/dva-react-demo/src/pages/componentAntdTableDva/models/FactoryHuose.js').default) });
app.model({ ...(require('M:/work/eclipse-workspace/dva-react-demo/src/pages/componentAntdTableDvaSimple/models/simpleModal.js').default) });

class DvaContainer extends Component {
  render() {
    app.router(() => this.props.children);
    return app.start()();
  }
}

export default DvaContainer;
