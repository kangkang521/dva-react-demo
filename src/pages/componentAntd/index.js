import React,{ Component } from 'react'
import { Table, Button, Row, Col  } from 'antd'

let houseName = null
let doorNum = null
let isChimney = false

export default class Home extends Component {
    state = {
      factory : [ ],
      }
      createHose = () => {
        const factory = this.state.factory
        factory.push({name: houseName.value,door: { number: doorNum.value }, isChimney: isChimney.value })
        this.setState({
          factory:[...factory],
        })
      }
  render (){
   
    const columns = [{
      title: '房子的名称',
      dataIndex: 'name',
      key: 'name',
    }, {
      title: '有几扇门',
      dataIndex: 'door.number',
      key: 'age',
    }, {
      title: '是否有烟筒',
      dataIndex: 'isChimney',
      key: 'address',
      render:(text)=><div>{text? '有':'没有'}</div>,
    },{
      title: '操作',
      dataIndex: 'isChimney',
      render:(record)=><Button type="danger" onClick={()=>{
        record.name
      }}>删除房子</Button>,
    },
  ]
      return (
        <div style={{ textAlign: 'center', backgroundColor: '#FFF' }}>
          <div>
          <Row>
            <Col xl={6} md={6} >房子名称：<input placeholder="请输入房子名称" ref={(_)=>houseName=_} style={{ width: 200 }}/></Col>
            <Col xl={6} md={6} >门的数量：<input placeholder="请输入门的数量" ref={(_)=>doorNum=_} style={{ width: 200 }}/></Col>
            <Col xl={6} md={6} >是否有烟筒：<select  placeholder="请选择烟筒"  ref={(_)=>isChimney=_} style={{ width: 200 }}><option value={false}>否</option><option value={true}>是</option></select></Col>
            <Col xl={6} md={6} > <Button type="dashed"  onClick={this.createHose}>建造房子</Button></Col>
          </Row>
           
           
          </div>
          <Table dataSource={this.state.factory} columns={columns} />
        </div>)
  }
}

