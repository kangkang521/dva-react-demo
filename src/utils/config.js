const api = '/api/system'
module.exports={
    /**
     * 修改房子的接口
     */
    updateFactoryHuoseById_: `${api}/updateFactoryHuose/:id`,
    /**
     * 修改房子的接口
     */
    addFactoryHuoseById_: `${api}/addFactoryHuose/:id`,
    /**
     * 修改房子的接口
     */
    deleteFactoryHuoseById_: `${api}/deleteFactoryHuose/:id`,
    /**
     * 根据id查询一个房子的接口
     */
    queryFactoryHuoseById_: `${api}/queryFactoryHuose/:id`,
    /**
     * 查询所有房子的接口
     */
    queryFactoryHuose_: `${api}/queryFactoryHuose`,
}