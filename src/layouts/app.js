/* global window */
/* global document */
import React from 'react'
import NProgress from 'nprogress'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { BackTop, Layout, Menu, Icon } from 'antd'
import { withRouter } from 'dva/router'
import Link from 'umi/link'
import '../themes/index.less'
import './app.less'
const { Content, Footer } = Layout
let lastHref

const SubMenu = Menu.SubMenu
const MenuItemGroup = Menu.ItemGroup

const App = ({ loading, app, children }) => {
  app
 const { href } = window.location

  if (lastHref !== href) {
    NProgress.start()
    if (!loading.global) {
      NProgress.done()
      lastHref = href
    }
  }

  return (
    <div>
      <Layout >
        <Layout style={{ height: '100vh', overflow: 'scroll' }} id="mainContainer">
          <BackTop target={() => document.getElementById('mainContainer')} />
          { true && <Menu mode="horizontal" >
            <Menu.Item key="mail">
              <Link to="/singleComponent"><Icon type="mail" />Hello world</Link>
            </Menu.Item>
            <SubMenu title={<span><Icon type="setting" />组件</span>}>
              <MenuItemGroup title="静态页面">
                <Menu.Item key="setting:1">
                  <Link to="/compositeComponent"><Icon type="mail" />复合组件</Link>
                </Menu.Item>
                <Menu.Item key="setting:2">
                  <Link to="/componentTransferValue"><Icon type="mail" />组件传值</Link>
                </Menu.Item>
                <Menu.Item key="setting:3">
                  <Link to="/componentState"><Icon type="mail" />组建状态</Link>
                </Menu.Item>
                <Menu.Item key="setting:4">
                  <Link to="/componentLifeCycle"><Icon type="mail" />组件生命周期</Link>
                </Menu.Item>
                <Menu.Item key="setting:5">
                  <Link to="/componentDecorate"><Icon type="mail" />组件样式</Link>
                </Menu.Item>
              </MenuItemGroup>
              <MenuItemGroup title="结合 Antd Dva">
                <Menu.Item key="setting:6">
                  <Link to="/componentAntd"><Icon type="mail" />结合 antd</Link>
                </Menu.Item>
                <Menu.Item key="setting:7">
                  <Link to="/componentAntdTable"><Icon type="mail" />结合 antd 多个组建</Link>
                </Menu.Item>
                <Menu.Item key="setting:8">
                  <Link to="/componentAntdTableDva"><Icon type="mail" />结合 Dva</Link>
                </Menu.Item>
                <Menu.Item key="setting:9">
                  <Link to="/componentAntdTableDvaSimple"><Icon type="mail" />简单的结合 Dva</Link>
                </Menu.Item>
              </MenuItemGroup>
            </SubMenu>
          </Menu>}
          <Content>
            {children}
          </Content>
          <Footer >
          </Footer>
        </Layout>
      </Layout>
    </div>
  )
}

App.propTypes = {
  app: PropTypes.object,
  loading: PropTypes.object,
}

export default withRouter(connect(({ app, loading }) => ({ app, loading }))(App))
